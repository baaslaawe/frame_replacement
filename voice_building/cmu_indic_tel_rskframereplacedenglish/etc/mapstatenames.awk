BEGIN { 
statenamemap[1] = 1;
statename[1] = "9r";
statenamemap[2] = 2;
statename[2] = "9r";
statenamemap[3] = 3;
statename[3] = "9r";
statenamemap[6] = 1;
statename[6] = "9r=";
statenamemap[7] = 2;
statename[7] = "9r=";
statenamemap[8] = 3;
statename[8] = "9r=";
statenamemap[11] = 1;
statename[11] = "A";
statenamemap[12] = 2;
statename[12] = "A";
statenamemap[13] = 3;
statename[13] = "A";
statenamemap[16] = 1;
statename[16] = "A:";
statenamemap[17] = 2;
statename[17] = "A:";
statenamemap[18] = 3;
statename[18] = "A:";
statenamemap[21] = 1;
statename[21] = "J";
statenamemap[22] = 2;
statename[22] = "J";
statenamemap[23] = 3;
statename[23] = "J";
statenamemap[26] = 1;
statename[26] = "N";
statenamemap[27] = 2;
statename[27] = "N";
statenamemap[28] = 3;
statename[28] = "N";
statenamemap[31] = 1;
statename[31] = "aI";
statenamemap[32] = 2;
statename[32] = "aI";
statenamemap[33] = 3;
statename[33] = "aI";
statenamemap[36] = 1;
statename[36] = "aU";
statenamemap[37] = 2;
statename[37] = "aU";
statenamemap[38] = 3;
statename[38] = "aU";
statenamemap[41] = 1;
statename[41] = "b";
statenamemap[42] = 2;
statename[42] = "b";
statenamemap[43] = 3;
statename[43] = "b";
statenamemap[46] = 1;
statename[46] = "bh";
statenamemap[47] = 2;
statename[47] = "bh";
statenamemap[48] = 3;
statename[48] = "bh";
statenamemap[51] = 1;
statename[51] = "c";
statenamemap[52] = 2;
statename[52] = "c";
statenamemap[53] = 3;
statename[53] = "c";
statenamemap[56] = 1;
statename[56] = "ch";
statenamemap[57] = 2;
statename[57] = "ch";
statenamemap[58] = 3;
statename[58] = "ch";
statenamemap[61] = 1;
statename[61] = "c}";
statenamemap[62] = 2;
statename[62] = "c}";
statenamemap[63] = 3;
statename[63] = "c}";
statenamemap[66] = 1;
statename[66] = "dB";
statenamemap[67] = 2;
statename[67] = "dB";
statenamemap[68] = 3;
statename[68] = "dB";
statenamemap[71] = 1;
statename[71] = "dBh";
statenamemap[72] = 2;
statename[72] = "dBh";
statenamemap[73] = 3;
statename[73] = "dBh";
statenamemap[76] = 1;
statename[76] = "dR";
statenamemap[77] = 2;
statename[77] = "dR";
statenamemap[78] = 3;
statename[78] = "dR";
statenamemap[81] = 1;
statename[81] = "dr";
statenamemap[82] = 2;
statename[82] = "dr";
statenamemap[83] = 3;
statename[83] = "dr";
statenamemap[86] = 1;
statename[86] = "e";
statenamemap[87] = 2;
statename[87] = "e";
statenamemap[88] = 3;
statename[88] = "e";
statenamemap[91] = 1;
statename[91] = "e:";
statenamemap[92] = 2;
statename[92] = "e:";
statenamemap[93] = 3;
statename[93] = "e:";
statenamemap[96] = 1;
statename[96] = "g";
statenamemap[97] = 2;
statename[97] = "g";
statenamemap[98] = 3;
statename[98] = "g";
statenamemap[101] = 1;
statename[101] = "gh";
statenamemap[102] = 2;
statename[102] = "gh";
statenamemap[103] = 3;
statename[103] = "gh";
statenamemap[106] = 1;
statename[106] = "hv";
statenamemap[107] = 2;
statename[107] = "hv";
statenamemap[108] = 3;
statename[108] = "hv";
statenamemap[111] = 1;
statename[111] = "i";
statenamemap[112] = 2;
statename[112] = "i";
statenamemap[113] = 3;
statename[113] = "i";
statenamemap[116] = 1;
statename[116] = "i:";
statenamemap[117] = 2;
statename[117] = "i:";
statenamemap[118] = 3;
statename[118] = "i:";
statenamemap[121] = 1;
statename[121] = "j";
statenamemap[122] = 2;
statename[122] = "j";
statenamemap[123] = 3;
statename[123] = "j";
statenamemap[126] = 1;
statename[126] = "k";
statenamemap[127] = 2;
statename[127] = "k";
statenamemap[128] = 3;
statename[128] = "k";
statenamemap[131] = 1;
statename[131] = "kh";
statenamemap[132] = 2;
statename[132] = "kh";
statenamemap[133] = 3;
statename[133] = "kh";
statenamemap[136] = 1;
statename[136] = "l";
statenamemap[137] = 2;
statename[137] = "l";
statenamemap[138] = 3;
statename[138] = "l";
statenamemap[141] = 1;
statename[141] = "lr";
statenamemap[142] = 2;
statename[142] = "lr";
statenamemap[143] = 3;
statename[143] = "lr";
statenamemap[146] = 1;
statename[146] = "m";
statenamemap[147] = 2;
statename[147] = "m";
statenamemap[148] = 3;
statename[148] = "m";
statenamemap[151] = 1;
statename[151] = "nB";
statenamemap[152] = 2;
statename[152] = "nB";
statenamemap[153] = 3;
statename[153] = "nB";
statenamemap[156] = 1;
statename[156] = "nr";
statenamemap[157] = 2;
statename[157] = "nr";
statenamemap[158] = 3;
statename[158] = "nr";
statenamemap[161] = 1;
statename[161] = "n~";
statenamemap[162] = 2;
statename[162] = "n~";
statenamemap[163] = 3;
statename[163] = "n~";
statenamemap[166] = 1;
statename[166] = "o";
statenamemap[167] = 2;
statename[167] = "o";
statenamemap[168] = 3;
statename[168] = "o";
statenamemap[171] = 1;
statename[171] = "o:";
statenamemap[172] = 2;
statename[172] = "o:";
statenamemap[173] = 3;
statename[173] = "o:";
statenamemap[176] = 1;
statename[176] = "p";
statenamemap[177] = 2;
statename[177] = "p";
statenamemap[178] = 3;
statename[178] = "p";
statenamemap[181] = 1;
statename[181] = "pau";
statenamemap[182] = 2;
statename[182] = "pau";
statenamemap[183] = 3;
statename[183] = "pau";
statenamemap[186] = 1;
statename[186] = "ph";
statenamemap[187] = 2;
statename[187] = "ph";
statenamemap[188] = 3;
statename[188] = "ph";
statenamemap[191] = 1;
statename[191] = "s";
statenamemap[192] = 2;
statename[192] = "s";
statenamemap[193] = 3;
statename[193] = "s";
statenamemap[196] = 5;
statename[196] = "pau";
statenamemap[199] = 1;
statename[199] = "tB";
statenamemap[200] = 2;
statename[200] = "tB";
statenamemap[201] = 3;
statename[201] = "tB";
statenamemap[204] = 1;
statename[204] = "tBh";
statenamemap[205] = 2;
statename[205] = "tBh";
statenamemap[206] = 3;
statename[206] = "tBh";
statenamemap[209] = 1;
statename[209] = "tR";
statenamemap[210] = 2;
statename[210] = "tR";
statenamemap[211] = 3;
statename[211] = "tR";
statenamemap[214] = 1;
statename[214] = "tr";
statenamemap[215] = 2;
statename[215] = "tr";
statenamemap[216] = 3;
statename[216] = "tr";
statenamemap[219] = 1;
statename[219] = "u";
statenamemap[220] = 2;
statename[220] = "u";
statenamemap[221] = 3;
statename[221] = "u";
statenamemap[224] = 1;
statename[224] = "u:";
statenamemap[225] = 2;
statename[225] = "u:";
statenamemap[226] = 3;
statename[226] = "u:";
statenamemap[229] = 1;
statename[229] = "v";
statenamemap[230] = 2;
statename[230] = "v";
statenamemap[231] = 3;
statename[231] = "v";
}
{ if (NF < 3)
     print $0;
  else
     printf("%s 125 %d %s\n",$1,statenamemap[$3],statename[$3])
}
